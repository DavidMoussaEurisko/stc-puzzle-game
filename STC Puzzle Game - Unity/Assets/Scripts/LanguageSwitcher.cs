﻿using UnityEngine;

public class LanguageSwitcher : MonoBehaviour
{
	public enum Language { English, Arabic }
	private static Language _currentLanguage;
	public static Language currentLanguage
	{
		get
		{
			return _currentLanguage;
		}
		set
		{
			_currentLanguage = value;

			switch(_currentLanguage)
			{
				case Language.Arabic:
					for (int i = 0; i < images.Length; i++)
						images[i].image.sprite = images[i].arabicSprite;

					for (int i = 0; i < texts.Length; i++)
						if (!texts[i].input)
						{
							texts[i].text.text = texts[i].placeholder ? texts[i].arabicText : ArabicFixerTool.FixLine(texts[i].arabicText);
							texts[i].text.lineSpacing = -1;
						}
					break;

				case Language.English:
					for (int i = 0; i < images.Length; i++)
						images[i].image.sprite = images[i].englishSprite;

					for (int i = 0; i < texts.Length; i++)
					{
						texts[i].text.text = texts[i].englishText;
						texts[i].text.lineSpacing = 1;
					}
					break;
			}
		}
	}

	private static LanguageDependantImage[] images;
	private static LanguageDependantText[] texts;

	private void Start()
	{
		images = (LanguageDependantImage[])Resources.FindObjectsOfTypeAll(typeof(LanguageDependantImage));
		texts = (LanguageDependantText[])Resources.FindObjectsOfTypeAll(typeof(LanguageDependantText));

		currentLanguage = (Language)PlayerPrefs.GetInt("Language", 1);
		PlayerPrefs.SetInt("Language", (int)currentLanguage);
	}

	public void ToggleLanguage()
	{
		if (currentLanguage == Language.English)
			currentLanguage = Language.Arabic;
		else
			currentLanguage = Language.English;

		PlayerPrefs.SetInt("Language", (int)currentLanguage);
	}
}
