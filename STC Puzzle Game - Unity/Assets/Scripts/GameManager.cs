﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public static GameManager instance = null;

	public InputField
		employmentNumberField,
		firstNameField,
		lastNameField,
		phoneNumberField;

	public GameObject leaderboardItem;
	public Transform leaderboardBody;

	public List<int> stars;

	private Dictionary<string, GameObject> _screens;
	public Dictionary<string, GameObject> screens
	{
		get
		{
			if (_screens == null)
			{
				_screens = new Dictionary<string, GameObject>();
				Transform screenParent = transform.Find("Canvas/Background");
				for (int i = 0; i < screenParent.childCount; i++)
					_screens.Add(screenParent.GetChild(i).name, screenParent.GetChild(i).gameObject);
			}

			return _screens;
		}
	}

	private bool waitingForInternet = true;

	private void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
	}

	private IEnumerator Start()
	{
		screens["Loader"].SetActive(true);
		WWW internetTest = new WWW("https://currentmillis.com/time/minutes-since-unix-epoch.php");
		yield return internetTest;
		if (internetTest.error != null)
		{
			screens["NoInternet"].SetActive(true);
			yield break;
		}
		screens["Loader"].SetActive(false);
		
		SliderManager.onStartedSolving.AddListener(() =>
		{
			SlideBlock[] blocks = FindObjectsOfType<SlideBlock>();
			for (int i = 0; i < blocks.Length; i++)
			{
				blocks[i].ToggleLogos();
			}
			screens["Loader"].SetActive(true);
		});
		SliderManager.onSolved.AddListener(() =>
		{
			SlideBlock[] blocks = FindObjectsOfType<SlideBlock>();
			for (int i = 0; i < blocks.Length; i++)
			{
				blocks[i].ToggleLogos();
			}
			screens["Loader"].SetActive(false);
		});
	}

	public void Exit()
	{
		Application.Quit();
	}

	public void SignUp()
	{
		if (employmentNumberField.text == "")
		{
			StartCoroutine(SignUpError());
			return;
		}

		screens["SignUp"].SetActive(false);
		screens["Loader"].SetActive(true);

		Database.SignUp(employmentNumberField.text, firstNameField.text, lastNameField.text, phoneNumberField.text, (response) =>
		{
			stars = new List<int>();

			screens["LevelSelect"].SetActive(true);
			screens["Loader"].SetActive(false);

			PlayerPrefs.SetString("ID", employmentNumberField.text);
			GetEmployee(employmentNumberField.text);
		});
	}

	private IEnumerator SignUpError()
	{
		screens["SignUpError"].SetActive(true);
		CanvasGroup group = screens["SignUpError"].GetComponent<CanvasGroup>();

		while (group.alpha < 0.9f)
		{
			group.alpha += Time.deltaTime * 4;
			yield return null;
		}

		yield return new WaitForSeconds(2);

		while (group.alpha > 0)
		{
			group.alpha -= Time.deltaTime * 4;
			yield return null;
		}

		screens["SignUpError"].SetActive(false);
	}

	public void GetEmployee(string employeeId)
	{
		screens["Main"].SetActive(false);
		screens["Loader"].SetActive(true);

		SliderManager.GameControllerObj.currLevel = 1;

		Database.GetEmployee(employeeId, (response) =>
		{
			Debug.Log(response.Print(true));
			
			screens["Loader"].SetActive(false);

			if (response["stages"].Count == 0)
			{
				screens["Main"].SetActive(false);
				screens["Loader"].SetActive(true);

				Database.SubmitStage(employeeId, "0", 0, (submitStageResponse) =>
				{
					stars = new List<int> { 0 };
					SliderManager.GameControllerObj.LoadLevels();
				});
			}
			else
			{
				stars = new List<int>();
				for (int i = 0; i < response["stages"].Count; i++)
				{
					for (int j = 0; j < response["stages"].Count; j++)
					{
						if (int.Parse(response["stages"][j]["stage_id"].str) == i)
							stars.Add((int)response["stages"][j]["stars"].i);
					}
				}
				SliderManager.GameControllerObj.LoadLevels();
			}

			screens["Loader"].SetActive(false);
		});
	}

	public void ResetLevelButtons()
	{
		employmentNumberField.text = "";
		firstNameField.text = "";
		lastNameField.text = "";
		phoneNumberField.text = "";

		for (int i = 0; i < SliderManager.GameControllerObj.levelContainer.transform.childCount; i++)
			Destroy(SliderManager.GameControllerObj.levelContainer.transform.GetChild(i).gameObject);
	}

	public void LoadLeaderboard()
	{
		screens["Loader"].SetActive(true);

		for (int i = 0; i < leaderboardBody.childCount; i++)
			Destroy(leaderboardBody.GetChild(i).gameObject);

		Database.GetLeaderboard((response) =>
		{
			for (int i = 0; i < response["data"].Count; i++)
			{
				GameObject item = Instantiate(leaderboardItem, leaderboardBody);

				string first = response["data"][i]["first_name"].str, last = response["data"][i]["last_name"].str;
				if (response["data"][i]["first_name"].str.Contains("\\u"))
					first = ArabicFixerTool.FixLine(DecodeEncodedNonAsciiCharacters(first));
				if (response["data"][i]["last_name"].str.Contains("\\u"))
				{
					last = ArabicFixerTool.FixLine(DecodeEncodedNonAsciiCharacters(last));
					item.transform.Find("SafeArea/Name/Last").SetAsFirstSibling();
				}

				if (first == "" && last == "")
					first = LanguageSwitcher.currentLanguage == LanguageSwitcher.Language.Arabic ? ArabicFixerTool.FixLine("مجهول") : "Unknown";

				item.transform.Find("SafeArea/Name/First").GetComponent<Text>().text = first;
				item.transform.Find("SafeArea/Name/Last").GetComponent<Text>().text = last;

				item.transform.Find("SafeArea/Stars").GetComponent<Text>().text = response["data"][i]["stars"].str;
			}

			screens["Leaderboard"].SetActive(true);

			screens["Loader"].SetActive(false);
		});
	}

	private static string DecodeEncodedNonAsciiCharacters(string value)
	{
		return Regex.Replace(
			value,
			@"\\u(?<Value>[a-zA-Z0-9]{4})",
			m =>
			{
				return ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString();
			});
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}
}