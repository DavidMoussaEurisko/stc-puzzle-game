﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelElement : MonoBehaviour
{

	public RectTransform lockedObject;
	public RectTransform unlockedObject;
	public List<Image> stars;
	public Text levelIndex;
	[SerializeField]
	private bool _unlocked;
	public bool unlocked
	{
		get
		{
			return _unlocked;
		}
		set
		{
			_unlocked = value;
			if (unlocked)
			{
				lockedObject.gameObject.SetActive(false);
				unlockedObject.gameObject.SetActive(true);
				GetComponent<Button>().interactable = true;
			}
			else
			{
				lockedObject.gameObject.SetActive(true);
				unlockedObject.gameObject.SetActive(false);
				GetComponent<Button>().interactable = false;
			}
		}
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	public void UnlockStar(int count)
	{
		for (int i = 0; i < count; i++)
		{
			stars[i].color = new Color(240, 174, 15, 255) / 255;
		}
		
	}

	public void StartLevel()
	{
		SliderManager.GameControllerObj.StartGame(Int32.Parse(levelIndex.text) - 1);
	}

}
