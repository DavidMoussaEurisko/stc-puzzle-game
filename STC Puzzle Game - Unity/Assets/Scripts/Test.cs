﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
	public Transform messages;

	private int i;

	private void Awake()
	{
		i = messages.childCount - 1;
	}

	public void Next()
	{
		messages.GetChild(i--).gameObject.SetActive(false);
	}
}
