﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class LanguageDependantImage : MonoBehaviour
{
	private Image _image;
	public Image image
	{
		get
		{
			if (!_image)
				_image = GetComponent<Image>();

			return _image;
		}
	}

	public Sprite englishSprite, arabicSprite;
}
