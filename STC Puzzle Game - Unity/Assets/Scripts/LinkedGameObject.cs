﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;

public class LinkedGameObject : MonoBehaviour
{
	public GameObject link;

	private void OnEnable()
	{
		link.SetActive(true);
	}
	private void OnDisable()
	{
		link.SetActive(false);
	}
}
