﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;

public class ButtonColor : MonoBehaviour
{
	private Button _button;
	private Button button
	{
		get
		{
			if (!_button)
				_button = GetComponent<Button>();

			return _button;
		}
	}

	private ProceduralImage _image;
	private ProceduralImage image
	{
		get
		{
			if (!_image)
				_image = GetComponent<ProceduralImage>();

			return _image;
		}
	}

	private Color iniColor;
	public Color disabledColor = new Color(192, 192, 192, 255) / 255;

	private void Start()
	{
		iniColor = image.color;
	}

	void Update()
	{
		if (button.interactable)
			image.color = iniColor;
		else
			image.color = disabledColor;
	}
}
