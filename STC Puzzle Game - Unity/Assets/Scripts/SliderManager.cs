﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour
{
	public struct Level
	{
		bool unlocked;
		int stars;
		bool crown;
	}

	public static Difficulty currDifficulty = Difficulty.Easy;

	public enum Difficulty
	{
		Easy,
		Medium,
		Hard
	}

	public float swipe = 10;

	public static UnityEvent onStartedSolving, onSolved;

	public AudioClip[] starSounds;

	public GameObject levelContainer;
	public GameObject levelSelectButton;
	//assign sprite with top left pivot
	public Sprite playerSprite, hintSprite;
	public Sprite[] logoSprites;

	//size of 1 box in 6*6 board for game
	public float boxSize = 76f;

	//Hint Arrow Sprite
	public GameObject HintArrow, stars, resultStars, gridFive, gridSix;

	public Text totalunlocked, currLevelText, currMovelbl, targetText, bestMoveText, scoreText, scoreTargetText, scoreBestText, scoreTextAr, scoreTargetTextAr, scoreBestTextAr, scoreCurLevelText, timeText;
	//Level clear and Loading Object

	public GameObject LevelClearobj;

	public int currLevel = 1;

	public static int TotalMove, TotalTime = 0;



	public GameObject[] screens;
	//Record each movemetn for undo operation
	public static List<blockdata> blockpositionList = new List<blockdata>();

	//Left top position of game to setup
	public Vector2 LeftTopPos;

	public static Vector2 LeftTopPositon;

	public static float BoxSize;

	public static BoardSolution sln = null;

	public static Board GameBoard;

	public static GameObject[] gameObjs;

	public static GameObject hintObj;

	public Button prevButton, nextButton, hintButton, undoButton, nextLevelButton;

	public static bool _HintMode;
	public bool HintMode
	{
		set
		{
			_HintMode = value;
			hintButton.interactable = !_HintMode;
		}
		get
		{
			return _HintMode;
		}
	}

	public struct blockdata
	{
		public int index;

		public Block block;
		public int SolutionBoardNo;

		public blockdata(int _index, Block _previousblock, Block _nextblock, int _SolutionBoardNo)
		{
			index = _index;
			block = _previousblock;
			SolutionBoardNo = _SolutionBoardNo;

		}
	}

	public static SliderManager GameControllerObj
	{
		get
		{
			return GameObject.Find("Slider Manager").GetComponent<SliderManager>();
		}
	}

	private int MaxLevel
	{
		get
		{
			if (currDifficulty == Difficulty.Easy)
				return Puzzle.BeginnerPuzzles().Count();
			else if (currDifficulty == Difficulty.Medium)
				return Puzzle.MediumPuzzles().Count();
			else
				return Puzzle.HardPuzzles().Count();
		}
	}


	public void ReloadLevel()
	{
		StopAllCoroutines();
		SetNewLevel(currLevel);
	}

	public void GiveHint()
	{
		if (!HintMode)
		{
			TotalMove = 0;
			SetCurrMove();
			StartCoroutine(GetSolution());
		}

	}

	public int playerProgress = 0;

	private void Awake()
	{
		onStartedSolving = new UnityEvent();
		onSolved = new UnityEvent();
	}

	void Start()
	{
		// ClearAllData();
		BoxSize = boxSize;
		LeftTopPositon = LeftTopPos;
		currMovelbl.text = TotalMove.ToString();
	}

	private void Update()
	{
		TimeSpan timeSpan = TimeSpan.FromSeconds(Time.time);
		string time = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
		timeText.text = time;
	}

	public void IncrementLevel()
	{
		if (playerProgress <= MaxLevel && levelContainer.transform.GetChild(currLevel + 1) != null)
		{
			if (levelContainer.transform.GetChild(currLevel + 1).GetComponent<LevelElement>().unlocked)
			{
				currLevel++;
				SetNewLevel(currLevel);
			}
		}
	}

	public void DecrementLevel()
	{
		if (playerProgress > 1)
		{
			currLevel--;
			SetNewLevel(currLevel);
		}
	}


	public void StartGame(int levelID)
	{
		GameManager.instance.screens["LevelSelect"].SetActive(false);
		GameManager.instance.screens["GameScreen"].SetActive(true);

		hintButton.interactable = true;
		undoButton.interactable = false;
		currLevel = levelID;
		GameBoard = GetPuzzle(levelID);
		SetPuzzle(GameBoard);
		UpdateText();
	}

	//intantiate blocks
	public void SetPuzzle(Board b)
	{
		if (Camera.main.aspect < 0.5625f)
			Camera.main.orthographicSize = (1f / Camera.main.aspect) * (9f / 16f) * 5f;

		boxSize = (b.size == 5) ? 99 : 82.5f;
		BoxSize = boxSize;
		gridFive.gameObject.SetActive((b.size == 5) ? true : false);
		gridSix.gameObject.SetActive((b.size == 6) ? true : false);

		if (currLevel > 0)
			prevButton.interactable = true;
		else
			prevButton.interactable = false;

		if (currLevel + 1 < MaxLevel)
		{
			if (levelContainer.transform.GetChild(currLevel + 1).GetComponent<LevelElement>().unlocked)
				nextButton.interactable = true;
			nextLevelButton.gameObject.SetActive(true);
		}
		else
		{
			nextButton.interactable = false;
			nextLevelButton.gameObject.SetActive(false);
		}


		// Sets the ingame stars
		for (int i = 0; i < stars.transform.childCount; i++)
		{
			if (i < GameManager.instance.stars[currLevel])
				stars.transform.GetChild(i).GetComponent<Image>().color = new Color(240, 174, 15, 255) / 255;
			else
				stars.transform.GetChild(i).GetComponent<Image>().color = new Color(0, 0, 0, 128) / 255;
		}

		StopGame();
		hintObj = InstantiateBlock(hintSprite, b._blocks[0]);
		hintObj.transform.position = new Vector3(500f, 500f, 1f);
		//hintObj.GetComponent<SpriteRenderer>().sortingOrder = -1;
		hintObj.GetComponent<SpriteRenderer>().color = new Color(216, 52, 116, 255) / 255;
		Destroy(hintObj.GetComponent<SlideBlock>());
		gameObjs = new GameObject[b._blocks.Length];
		for (int i = 0; i < b._blocks.Length; i++)
		{
			GameObject gTemp;
			if (i == 0)
			{
				gTemp = InstantiateBlock(playerSprite, b._blocks[i]);
			}
			else
			{
				if (b.Blocks[i].Orientation == BlockOrientation.Orientation.Horizontal)
				{
					if (b.Blocks[i].Length == 2)
					{
						gTemp = InstantiateBlock(playerSprite, b._blocks[i]);

					}
					else
					{
						gTemp = InstantiateBlock(playerSprite, b._blocks[i]);
					}
				}
				else
				{
					if (b.Blocks[i].Length == 2)
					{
						gTemp = InstantiateBlock(playerSprite, b._blocks[i]);
					}
					else
					{
						gTemp = InstantiateBlock(playerSprite, b._blocks[i]);
					}
				}
			}

			gTemp.GetComponent<SlideBlock>().SetLogo(i == 0, b.Blocks[i].Orientation == BlockOrientation.Orientation.Vertical, b.Blocks[i].Length);

			//    gTemp.transform.position = setBlockPosition(b.Blocks[i]);
			gTemp.GetComponent<SpriteRenderer>().color = (i == 0) ? new Color(70, 70, 70, 255) / 255 : new Color(255, 255, 255, 255) / 255;
			gTemp.AddComponent<BlockMovement>();
			gTemp.GetComponent<BlockMovement>().thisBlock = b.Blocks[i];
			gTemp.GetComponent<BlockMovement>().thisblockId = i;
			gTemp.GetComponent<BlockMovement>().startPos = LeftTopPos;
			gTemp.GetComponent<BoxCollider2D>().size = gTemp.GetComponent<SpriteRenderer>().size;
			gTemp.GetComponent<BoxCollider2D>().offset = Vector2.Scale(gTemp.GetComponent<SpriteRenderer>().size / 2, (Vector2.right + Vector2.down));
			gTemp.name = i.ToString();

			gTemp.tag = "Game";
			gameObjs[i] = gTemp;

			if (gTemp.transform.childCount < 1)
			{
				GameObject shadow = new GameObject();
				shadow.transform.parent = gTemp.transform;
				shadow.transform.position = gTemp.transform.position + Vector3.down * 0.14f / 2 + Vector3.forward * 0.01f;
				shadow.transform.rotation = gTemp.transform.rotation;
				shadow.AddComponent<SpriteRenderer>();
				shadow.GetComponent<SpriteRenderer>().sprite = gTemp.GetComponent<SpriteRenderer>().sprite;
				shadow.GetComponent<SpriteRenderer>().drawMode = gTemp.GetComponent<SpriteRenderer>().drawMode;
				shadow.GetComponent<SpriteRenderer>().size = gTemp.GetComponent<SpriteRenderer>().size;
				shadow.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.2f);
			}
			/*
			GameObject Logo = new GameObject();
			Logo.transform.parent = gTemp.transform;
			Logo.transform.position = gTemp.transform.position + Vector3.back * 0.01f + new Vector3(gTemp.GetComponent<SpriteRenderer>().size.x / 2, -gTemp.GetComponent<SpriteRenderer>().size.y / 2, 0);
			Logo.transform.rotation = gTemp.transform.rotation;
			Logo.AddComponent<SpriteRenderer>();
			Logo.GetComponent<SpriteRenderer>().sprite = logoSprites[(i == 0) ? i : UnityEngine.Random.Range(1, logoSprites.Length)];
			Logo.GetComponent<SpriteRenderer>().drawMode = gTemp.GetComponent<SpriteRenderer>().drawMode;
			Logo.GetComponent<SpriteRenderer>().size = Logo.GetComponent<SpriteRenderer>().size / Logo.GetComponent<SpriteRenderer>().size.y * gTemp.GetComponent<SpriteRenderer>().size.x / 3;
			*/
		}
		UpdateText();
	}

	public void UpdateText()
	{
		currLevelText.text = LanguageSwitcher.currentLanguage == LanguageSwitcher.Language.English ?
			"Level: " + (currLevel + 1).ToString("00") :
			(currLevel + 1).ToString("00") + ArabicFixerTool.FixLine("المرحلة: ");

		targetText.text = LanguageSwitcher.currentLanguage == LanguageSwitcher.Language.English ?
			"Target: " + PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Target", 0).ToString() :
			PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Target", 0).ToString() + ArabicFixerTool.FixLine("الهدف: ");

		bestMoveText.text =
			LanguageSwitcher.currentLanguage == LanguageSwitcher.Language.English ?
			"Best: " + PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Best", 0).ToString() :
			PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Best", 0).ToString() + ArabicFixerTool.FixLine("الأفضل: ");

		currMovelbl.text = TotalMove.ToString();
	}

	public void StopGame()
	{
		HintMode = false;
		TotalMove = 0;
		GameObject[] oldgameObjs = GameObject.FindGameObjectsWithTag("Game");
		foreach (GameObject gTemp in oldgameObjs)
		{
			Destroy(gTemp);
		}
		if (hintObj != null)
		{
			Destroy(hintObj);
		}
		UpdateText();
	}

	//get 3d position according to 6*6 board position
	public static Vector3 GetBlockPosition(Block _block)
	{

		Vector3 Position = Vector3.zero;
		if (_block.Orientation == BlockOrientation.Orientation.Horizontal)
		{
			Position = new Vector3(LeftTopPositon.x + _block.Column * (BoxSize / 100), LeftTopPositon.y - _block.Row * (BoxSize / 100), 0f);
		}
		else
		{
			Position = new Vector3(LeftTopPositon.x + _block.Column * (BoxSize / 100), LeftTopPositon.y - _block.Row * (BoxSize / 100), 0f);
		}

		return Position;
	}

	public static GameObject InstantiateBlock(Sprite _Sprite, Block block, bool Changeobj = false, GameObject ObjtoChange = null)
	{
		float gap = 0.14f;
		GameObject gTemp;
		SpriteRenderer spriteRenderer;
		if (!Changeobj)
		{
			if (_Sprite.name != "Hint")
			{
				gTemp = Instantiate(Resources.Load<GameObject>("Block"));
				spriteRenderer = gTemp.GetComponent<SpriteRenderer>();
			}
			else
			{
				gTemp = new GameObject();
				spriteRenderer = gTemp.AddComponent<SpriteRenderer>();
			}
			spriteRenderer.sprite = _Sprite;
			spriteRenderer.drawMode = SpriteDrawMode.Sliced;
			spriteRenderer.size = Vector2.one * ((GameBoard.size == 5) ? 0.85f : 0.685f);
		}

		else
		{
			gTemp = ObjtoChange;
			spriteRenderer = gTemp.GetComponent<SpriteRenderer>();
			spriteRenderer.drawMode = SpriteDrawMode.Sliced;
			spriteRenderer.size = Vector2.one * ((GameBoard.size == 5) ? 0.85f : 0.685f);
		}
		if (block.Orientation == BlockOrientation.Orientation.Horizontal)
		{
			gTemp.transform.localRotation = Quaternion.Euler(Vector3.zero);
			//gTemp.transform.localScale = new Vector3((BoxSize * block.Length) / spriteRenderer.sprite.texture.width, BoxSize / spriteRenderer.sprite.texture.height, 1f);
			spriteRenderer.size = new Vector2(spriteRenderer.size.x * block.Length + (gap * (block.Length - 1)), spriteRenderer.size.y);
		}
		else
		{
			gTemp.transform.localRotation = Quaternion.Euler(new Vector3(0f, 180f, -90f));
			//gTemp.transform.localScale = new Vector3((BoxSize * block.Length) / spriteRenderer.sprite.texture.width, BoxSize / spriteRenderer.sprite.texture.height, 1f);
			spriteRenderer.size = new Vector2(spriteRenderer.size.x * block.Length + (gap * (block.Length - 1)), spriteRenderer.size.y);
		}


		gTemp.name = spriteRenderer.sprite.name;
		gTemp.transform.position = GetBlockPosition(block);

		return gTemp;
	}

	//return solution(hint) for current game
	public IEnumerator GetSolution()
	{
		onStartedSolving.Invoke();

		object lockHandle = new System.Object();
		bool done = false;
		yield return null;
		var myThread = new System.Threading.Thread(() =>
		{

			sln = Puzzle.FindSolutionBFS(GameBoard);
			lock (lockHandle)
			{
				done = true;
			}
		});
		myThread.Start();


		while (true)
		{

			yield return null;
			lock (lockHandle)
			{
				if (done)
				{
					break;
				}
			}
		}

		SetHint();
		HintMode = true;

		onSolved.Invoke();
	}

	public void SetCurrMove()
	{
		TotalMove++;
		currMovelbl.text = TotalMove.ToString();
		if (TotalMove > 0)
		{
			undoButton.interactable = true;
			hintButton.interactable = false;
		}
	}

	public void SetNewLevel(int LevelNo)
	{
		undoButton.interactable = false;
		HintArrow.transform.parent = this.transform;
		HintMode = false;
		HintArrow.transform.position = new Vector3(500f, 500f, 1f);
		blockpositionList.Clear();
		GameBoard = GetPuzzle(currLevel);
		SetPuzzle(GameBoard);
		TotalMove = 0;
		UpdateText();
		currMovelbl.text = TotalMove.ToString();
	}

	public void Undo()
	{
		if (blockpositionList.Count > 0)
		{

			blockdata _blockdata = blockpositionList[blockpositionList.Count - 1];
			gameObjs[_blockdata.index].transform.position = GetBlockPosition(_blockdata.block);
			GameBoard._blocks[_blockdata.index] = _blockdata.block;
			gameObjs[_blockdata.index].GetComponent<BlockMovement>().thisBlock = _blockdata.block;
			blockpositionList.RemoveAt(blockpositionList.Count - 1);

			if (HintMode)
			{
				TotalMove = _blockdata.SolutionBoardNo;
				SetHint();
			}
			else
			{
				if (TotalMove > 0)
				{
					TotalMove--;
				}

			}
			currMovelbl.GetComponent<Text>().text = TotalMove.ToString();
		}
		if (!(TotalMove > 0))
		{
			undoButton.interactable = false;
			if (!HintMode)
			{
				hintButton.interactable = true;
			}
		}
	}

	public void LevelClear()
	{
		print("LEVEL CLEAR");
		int totalStars = 1;
		if (TotalMove <= Mathf.Floor(PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Target", 0) * 1.5f) && !HintMode)
		{
			totalStars++;
			if (TotalMove <= Mathf.Floor(PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Target", 0)))
				totalStars++;
		}
		
		SaveStars(currLevel, totalStars);

		for (int i = 0; i < resultStars.transform.childCount; i++)
		{
			resultStars.transform.GetChild(i).GetComponent<Image>().color = new Color(192, 192, 192, 255) / 255;
			resultStars.transform.GetChild(i).GetComponent<Shadow>().enabled = false;
		}
		scoreText.text = TotalMove.ToString();
		scoreTargetText.text = PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Target", 0).ToString();

		scoreTextAr.text = scoreText.text;
		scoreTargetTextAr.text = scoreTargetText.text;

		scoreCurLevelText.text = LanguageSwitcher.currentLanguage == LanguageSwitcher.Language.English ?
			"Level: " + (currLevel + 1).ToString("00") :
			(currLevel + 1).ToString("00") + ArabicFixerTool.FixLine("المرحلة: ");

		scoreBestText.text = GetBestScore();
		scoreBestTextAr.text = scoreBestText.text;

		SlideBlock[] blocks = FindObjectsOfType<SlideBlock>();
		for (int i = 0; i < blocks.Length; i++)
		{
			blocks[i].DestroyLogos();
		}

		GameObject levelMessage = Instantiate(Resources.Load<GameObject>("Messages/Message " + Random.Range(1, 13)), transform.parent.Find("Canvas/Background"));
		levelMessage.transform.Find("Panel/ViewScore").GetComponent<Button>().onClick.AddListener(() =>
		{
			UnlockNextLevel();
			LevelClearobj.SetActive(true);

			if (LanguageSwitcher.currentLanguage == LanguageSwitcher.Language.Arabic)
			{
				LevelClearobj.transform.Find("Panel/Score-en").gameObject.SetActive(false);
				LevelClearobj.transform.Find("Panel/Score-ar").gameObject.SetActive(true);
			}
			else
			{
				LevelClearobj.transform.Find("Panel/Score-en").gameObject.SetActive(true);
				LevelClearobj.transform.Find("Panel/Score-ar").gameObject.SetActive(false);
			}

			StartCoroutine(AnimateWin(totalStars));

			Destroy(levelMessage);
		});

		HintArrow.transform.parent = transform;
	}

	private IEnumerator AnimateWin(int totalStars)
	{
		while (GameManager.instance.screens["Loader"].activeInHierarchy)
			yield return null;

		int earnedStars = 0;
		for (int i = 0; i < resultStars.transform.childCount; i++)
		{
			if (i < totalStars)
			{
				resultStars.transform.GetChild(i).GetComponent<Image>().color = new Color(240, 174, 15, 255) / 255;
				resultStars.transform.GetChild(i).GetComponent<Shadow>().enabled = true;

				GetComponent<AudioSource>().PlayOneShot(starSounds[earnedStars]);
				earnedStars++;
			}

			yield return new WaitForSeconds(.5f);
		}
	}

	private string GetBestScore()
	{
		if (PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Best", 999) > TotalMove)
		{
			PlayerPrefs.SetInt("Level_" + currLevel.ToString() + "_Best", TotalMove);
			PlayerPrefs.Save();
		}
		return PlayerPrefs.GetInt("Level_" + currLevel.ToString() + "_Best", 0).ToString();


	}

	public void SetHint()
	{
		Block newBlock, oldBlock;
		var blocks = sln.Moves.ToArray()[TotalMove]._blocks.Except(GameBoard._blocks);

		if (blocks.Count() > 0)
		{
			newBlock = blocks.ElementAt(0); //Check next chnage in solution board
			oldBlock = GameBoard._blocks.Except(sln.Moves.ToArray()[TotalMove]._blocks).ElementAt(0);
			SetHintObjArrows(newBlock, oldBlock);
		}
	}

	void SetHintObjArrows(Block _newBlock, Block _oldBlock)
	{
		var objs = gameObjs.Where(g => g.transform.position == GetBlockPosition(_oldBlock));

		if (objs.Count() > 0)
		{
			GameObject CurrHintObj = objs.ElementAt(0); //Get change in current board for show hint
			foreach (GameObject gTemp in gameObjs)
			{
				gTemp.GetComponent<BlockMovement>().isthiscurrHintObj = false;
			}
			CurrHintObj.GetComponent<BlockMovement>().isthiscurrHintObj = true; //Set currObject as hint object 
			InstantiateBlock(hintSprite, _newBlock, true, hintObj); //Adjust hint object according to next hint;
			HintArrow.transform.position = CurrHintObj.GetComponent<Renderer>().bounds.center;
			HintArrow.transform.parent = null;
			if (_newBlock.Orientation == BlockOrientation.Orientation.Horizontal)
			{
				if (GetBlockPosition(_newBlock).x > GetBlockPosition(_oldBlock).x)
				{
					HintArrow.transform.localRotation = Quaternion.Euler(Vector3.zero);
				}
				else
				{
					HintArrow.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);
				}
			}
			else
			{
				if (GetBlockPosition(_newBlock).y > GetBlockPosition(_oldBlock).y)
				{
					HintArrow.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);

				}
				else
				{
					HintArrow.transform.localRotation = Quaternion.Euler(0f, 180f, 270f);
				}
			}
			HintArrow.transform.localScale = Vector3.one;
			HintArrow.transform.parent = CurrHintObj.transform;
		}
	}

	Board GetPuzzle(int levelno)
	{
		Board b = new Board();
		if (currDifficulty == Difficulty.Easy)
			b = Puzzle.BeginnerPuzzles().ElementAt(levelno);
		else if (currDifficulty == Difficulty.Medium)
			b = Puzzle.MediumPuzzles().ElementAt(levelno);
		else
			b = Puzzle.HardPuzzles().ElementAt(levelno);
		return b;
	}

	public void LoadLevels()
	{
		StartCoroutine(LoadLevelsAsync());
	}

	public IEnumerator LoadLevelsAsync()
	{
		GameManager.instance.screens["Loader"].SetActive(true);

		yield return null;

		for (int i = 0; i < MaxLevel; i++)
		{
			GameObject levelButton = Instantiate(levelSelectButton, levelContainer.transform);
			levelButton.GetComponent<LevelElement>().levelIndex.text = (i + 1).ToString();
		}

		UpdateLevels();

		GameManager.instance.screens["Loader"].SetActive(false);
	}

	public void UpdateLevels()
	{
		playerProgress = GameManager.instance.stars.Count;
		Debug.Log(playerProgress);
		totalunlocked.text = playerProgress.ToString("00") + "/" + MaxLevel.ToString("00");
		for (int i = 0; i < levelContainer.transform.childCount; i++)
		{
			LevelElement level = levelContainer.transform.GetChild(i).GetComponent<LevelElement>();
			if (i < playerProgress)
			{
				Debug.Log("unlocked " + i);
				level.unlocked = true;
				level.UnlockStar(GameManager.instance.stars[i]);
			}
			else
				level.unlocked = false;
		}
	}

	public void SaveStars(int levelId, int count)
	{
		if (GameManager.instance.stars[levelId] < count)
		{
			GameManager.instance.stars[levelId] = count;
			Database.SubmitStage(PlayerPrefs.GetString("ID"), levelId.ToString(), count, (res) =>
			{
				UpdateLevels();
			});
		}
	}

	public void UnlockNextLevel()
	{
		if (currLevel + 1 >= GameManager.instance.stars.Count && playerProgress < MaxLevel)
		{
			GameManager.instance.screens["Loader"].SetActive(true);
			Database.SubmitStage(PlayerPrefs.GetString("ID"), (currLevel + 1).ToString(), 0, (res) =>
			{
				GameManager.instance.stars.Add(0);
				UpdateLevels();
				GameManager.instance.screens["Loader"].SetActive(false);
			});
		}
	}


	private void ClearAllData()
	{
		PlayerPrefs.DeleteAll();
	}

	public void ResetContainer()
	{
		levelContainer.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
	}
}
