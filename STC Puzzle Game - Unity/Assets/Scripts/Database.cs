﻿using System;
using System.Collections;
using UnityEngine;

public class Database : MonoBehaviour
{
	public string token;

	private static Database instance;

	private void Awake()
	{
		if (instance == null)
			instance = this;
	}

	private WWWForm STCForm()
	{
		WWWForm form = new WWWForm();
		form.AddField("token", token);
		return form;
	}

	public static void SignUp(string employeeId, string firstName, string lastName, string phoneNumber, Action<JSONObject> callback = null)
	{
		instance.StartCoroutine(instance.SignUpAsync(employeeId, firstName, lastName, phoneNumber, callback));
	}

	private IEnumerator SignUpAsync(string employeeId, string firstName, string lastName, string phoneNumber, Action<JSONObject> callback)
	{
		WWWForm form = STCForm();
		form.AddField("id", employeeId);
		form.AddField("first_name", firstName);
		form.AddField("last_name", lastName);
		form.AddField("phone_number", phoneNumber);

		WWW post = new WWW("http://euriskomobility.me/stc-slider-game/panel/api/employee/save_data.php", form);
		yield return post;

		if (callback != null)
			callback(new JSONObject(post.text));
	}

	public static void GetEmployee(string employeeId, Action<JSONObject> callback = null)
	{
		instance.StartCoroutine(instance.GetEmployeeAsync(employeeId, callback));
	}

	private IEnumerator GetEmployeeAsync(string employeeId, Action<JSONObject> callback)
	{
		WWWForm form = STCForm();
		form.AddField("id", employeeId);

		WWW post = new WWW("http://euriskomobility.me/stc-slider-game/panel/api/employee/get_data.php", form);
		yield return post;

		if (callback != null)
			callback(new JSONObject(post.text));
	}

	public static void SubmitStage(string employeeId, string levelId, int stars, Action<JSONObject> callback = null)
	{
		instance.StartCoroutine(instance.SubmitStageAsync(employeeId, levelId, stars, callback));
	}

	private IEnumerator SubmitStageAsync(string employeeId, string levelId, int stars, Action<JSONObject> callback)
	{
		WWWForm form = STCForm();
		form.AddField("employee_id", employeeId);
		form.AddField("stage_id", levelId);
		form.AddField("stars", stars);

		WWW post = new WWW("http://euriskomobility.me/stc-slider-game/panel/api/stage/submit_data.php", form);
		yield return post;

		if (callback != null)
			callback(new JSONObject(post.text));
	}

	public static void GetLeaderboard(Action<JSONObject> callback = null)
	{
		instance.StartCoroutine(instance.GetLeaderboardAsync(callback));
	}

	private IEnumerator GetLeaderboardAsync(Action<JSONObject> callback)
	{
		WWWForm form = STCForm();

		WWW post = new WWW("http://euriskomobility.me/stc-slider-game/panel/api/employee/get_leaderboard.php", form);
		yield return post;

		if (callback != null)
			callback(new JSONObject(post.text));
	}
}
