﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SlideBlock : MonoBehaviour
{
	private static int logoCount = 8;
	private static int logoIndex = -1;

	private void Awake()
	{
		if (logoIndex == -1)
			logoIndex = Random.Range(0, logoCount);
	}

	public void SetLogo(bool player, bool vertical, int length)
	{
		Transform logoParent;
		if (player)
		{
			logoParent = transform.Find("Player");
			logoParent.localScale *= SliderManager.GameBoard.size == 5 ? 0.35f : 0.3f;

			SpriteRenderer logo = null;
			if (LanguageSwitcher.currentLanguage == LanguageSwitcher.Language.Arabic)
			{
				logo = FindObject(logoParent, "ar").GetComponent<SpriteRenderer>();
				logo.gameObject.SetActive(true);
			}
			else
			{
				logo = FindObject(logoParent, "en").GetComponent<SpriteRenderer>();
				logo.gameObject.SetActive(true);
			}

			logo.transform.localPosition += Vector3.right * ((length == 2 ? 5.25f : 7.78f) / 2 - (logo.size.x / 2) * logo.transform.localScale.x);
			logo.transform.localPosition += Vector3.down * logo.transform.localScale.y / 2;
		}
		else
		{
			logoParent = transform.Find("Blocks");
			logoParent.localScale *= SliderManager.GameBoard.size == 5 ? 0.35f : 0.3f;
			
			SpriteRenderer logo = logoParent.GetChild(logoIndex).GetComponent<SpriteRenderer>();

			if (logoIndex >= logoCount - 1)
				logoIndex = 0;
			else
				logoIndex++;

			logo.gameObject.SetActive(true);

			if (vertical)
			{
				logo.flipX = true;

				logo.transform.localPosition += Vector3.right * ((length == 2 ? 5.25f : 7.78f) / 2 + (logo.size.x / 2) * logo.transform.localScale.x);
			}
			else
			{
				logo.transform.localPosition += Vector3.right * ((length == 2 ? 5.25f : 7.78f) / 2 - (logo.size.x / 2) * logo.transform.localScale.x);
			}

			logo.transform.localPosition += Vector3.down * logo.transform.localScale.y;
		}
	}

	public void DestroyLogos()
	{
		Destroy(transform.Find("Blocks").gameObject);
		Destroy(transform.Find("Player").gameObject);
	}

	public void ToggleLogos()
	{
		transform.Find("Blocks").gameObject.SetActive(!transform.Find("Blocks").gameObject.activeSelf);
		transform.Find("Player").gameObject.SetActive(!transform.Find("Player").gameObject.activeSelf);
	}

	public static GameObject FindObject(Transform parent, string name)
	{
		Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
		foreach (Transform t in trs)
			if (t.name == name)
				return t.gameObject;

		return null;
	}
}
