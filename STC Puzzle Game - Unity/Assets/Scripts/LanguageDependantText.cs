﻿using UnityEngine;
using UnityEngine.UI;

public class LanguageDependantText : MonoBehaviour
{
	private Text _text;
	public Text text
	{
		get
		{
			if (!_text)
				_text = GetComponent<Text>();

			return _text;
		}
	}

	public bool input, placeholder;
	public string englishText, arabicText;

	private void Start()
	{
		switch (LanguageSwitcher.currentLanguage)
		{
			case LanguageSwitcher.Language.Arabic:
				text.text = placeholder ? arabicText : ArabicFixerTool.FixLine(arabicText);
				text.lineSpacing = -1;
				break;

			case LanguageSwitcher.Language.English:
				text.text = englishText;
				break;
		}
	}

	public void OnEdit()
	{
		text.text = ArabicFixerTool.FixLine(text.text);
	}
}
