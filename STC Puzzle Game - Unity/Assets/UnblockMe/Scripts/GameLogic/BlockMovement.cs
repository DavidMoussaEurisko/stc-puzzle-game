﻿using UnityEngine;
using System.Collections;
//Apply this Script On Each Block To Move LEFT,RIGHT,UP,DOWN



[RequireComponent(typeof(BoxCollider2D))]
public class BlockMovement : MonoBehaviour
{

	public bool isthiscurrHintObj;

	[HideInInspector]
	public Block thisBlock, previousBlock;
	[HideInInspector]
	public int thisblockId;
	private float Left, Right, Up, Down;
	bool SwipeStart, LerpStart;
	public Vector2 startPos, startMousePos;
	public Vector3 swipeStartPos, swipeEndPos;
	public float startMouseTime, speed, startTime, EndTime, LerpStartTime;
	Vector3 currPos;
	Block[] currblocks;
	// Use this for initialization
	void OnEnable()
	{

		SwipeStart = false;
		EasyTouch.On_SwipeStart += On_SwipeStart;
		EasyTouch.On_Swipe += On_Swipe;
		EasyTouch.On_SwipeEnd += On_SwipeEnd;
	}

	void OnDisable()
	{
		EasyTouch.On_SwipeStart -= On_SwipeStart;
		EasyTouch.On_Swipe -= On_Swipe;
		EasyTouch.On_SwipeEnd -= On_SwipeEnd;
	}


	void On_SwipeStart(Gesture g)
	{

		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
		RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
		if (hit.collider != null)
		{
			if (hit.collider.gameObject == this.transform.gameObject)
			{
				if (!SliderManager._HintMode || (SliderManager._HintMode && isthiscurrHintObj))
				{
					SwipeStart = true;
					currPos = transform.position;
					startMouseTime = Time.time;
					startMousePos = GetTouchPoint();
					//Count Left,Right,Up,down for selected block
					Left = currPos.x - SliderManager.GameBoard.CalculateMovement(thisblockId).Left * SliderManager.BoxSize / 100f;
					Right = currPos.x + SliderManager.GameBoard.CalculateMovement(thisblockId).Right * SliderManager.BoxSize / 100f;
					Up = currPos.y + SliderManager.GameBoard.CalculateMovement(thisblockId).Up * SliderManager.BoxSize / 100f;
					Down = currPos.y - SliderManager.GameBoard.CalculateMovement(thisblockId).Down * SliderManager.BoxSize / 100f;

				}

			}
		}
	}

	void On_Swipe(Gesture g)
	{
		//Change clamped position according to mouse position or touch position of selected object
		if (SwipeStart && !LerpStart)
		{
			if (thisBlock.Orientation == BlockOrientation.Orientation.Horizontal)
			{

				if (g.swipe == EasyTouch.SwipeType.Left || g.swipe == EasyTouch.SwipeType.Right)
				{

					transform.position = new Vector3(Mathf.Clamp(transform.position.x + ((GetTouchPoint().x - startMousePos.x) / SliderManager.GameControllerObj.swipe), Left, Right), transform.position.y, 0f);



				}

			}
			else
			{
				if (g.swipe == EasyTouch.SwipeType.Up || g.swipe == EasyTouch.SwipeType.Down)
				{
					transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y + ((GetTouchPoint().y - startMousePos.y) / SliderManager.GameControllerObj.swipe), Down, Up), 0f);
				}
			}
		}
	}

	void On_SwipeEnd(Gesture g)
	{

		//Changed value after drag..
		if (SwipeStart && !LerpStart)
		{


			int Value;
			if (thisBlock.Orientation == BlockOrientation.Orientation.Horizontal)
			{
				speed = (GetTouchPoint().x - startMousePos.x) / (Time.time - startMouseTime > 0 ? Time.time - startMouseTime : 0.1f) / 10f;

				Value = Mathf.RoundToInt(Mathf.Abs(startPos.x - Mathf.Clamp(transform.position.x + speed, Left, Right)) / (SliderManager.BoxSize / 100f));

				swipeStartPos = transform.position;
				LerpStart = true;
				LerpStartTime = Time.time;

				previousBlock = thisBlock;
				thisBlock = SliderManager.GameBoard.AddNewValue(thisblockId, Value > 5 ? 5 : Value);
			}
			else if (thisBlock.Orientation == BlockOrientation.Orientation.Vertical)
			{
				speed = (GetTouchPoint().y - startMousePos.y) / (Time.time - startMouseTime > 0 ? Time.time - startMouseTime : 0.1f) / 10f;

				LerpStart = true;
				LerpStartTime = Time.time;
				Value = Mathf.RoundToInt(Mathf.Abs(startPos.y - Mathf.Clamp(transform.position.y + speed, Down, Up)) / (SliderManager.BoxSize / 100f));
				swipeStartPos = transform.position;

				previousBlock = thisBlock;
				thisBlock = SliderManager.GameBoard.AddNewValue(thisblockId, Value > 5 ? 5 : Value);
			}
			swipeEndPos = SliderManager.GetBlockPosition(thisBlock);


			//change in Position

			GetComponent<AudioSource>().Play();


		}


	}


	//Return 3d position of current touch position
	Vector3 GetTouchPoint()
	{
		return Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
	}

	void Update()
	{

		if (LerpStart)
		{
			transform.position = Vector3.Lerp(swipeStartPos, swipeEndPos, (Time.time - LerpStartTime) / 0.2f);
		}
		if (LerpStart && (Time.time - LerpStartTime) > 0.2f)
		{
			LerpStart = false;
			if (transform.position != currPos)
			{
				if (!SliderManager._HintMode)
					SliderManager.GameControllerObj.SetCurrMove();
				int index = thisblockId;
				Block prev = previousBlock;

				SliderManager.blockdata _blockdata = new SliderManager.blockdata();
				_blockdata.index = index;
				_blockdata.block = prev;
				_blockdata.SolutionBoardNo = SliderManager.TotalMove;

				SliderManager.blockpositionList.Add(_blockdata);
				if (SliderManager.GameBoard._blocks[0]._col > ((SliderManager.GameBoard.size == 5) ? 3 : 4))
				{
					SliderManager.GameControllerObj.LevelClear();

				}
				else if (SliderManager._HintMode && isthiscurrHintObj)
				{
					print("HERE ");
					if (transform.position == SliderManager.hintObj.transform.position)
					{

						isthiscurrHintObj = false;


						SliderManager.GameControllerObj.SetCurrMove();





					}
					SliderManager.GameControllerObj.SetHint();
				}
			}
			SwipeStart = false;
		}
	}
}
