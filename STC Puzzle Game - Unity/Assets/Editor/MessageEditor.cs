﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;

public class MessageEditor : EditorWindow
{
	Color newColor;
	string newMessage;

	[MenuItem("STC/Message Color Changer")]
	static void Init()
	{
		MessageEditor window = (MessageEditor)GetWindow(typeof(MessageEditor));
		window.Show();
	}

	void OnGUI()
	{
		newColor = EditorGUILayout.ColorField("New Color", newColor);
		newMessage = EditorGUILayout.TextArea(newMessage, GUILayout.Height(100));
		if (GUILayout.Button("Update Color"))
			UpdateColor();
	}

	void UpdateColor()
	{
		ProceduralImage[] images = Selection.activeGameObject.GetComponentsInChildren<ProceduralImage>();

		for (int i = 0; i < images.Length; i++)
		{
			if (images[i].color == Color.white)
				continue;

			images[i].color = newColor;
		}

		string messageName = Selection.activeGameObject.name;
		Selection.activeTransform.Find("Panel/Message/Icon").GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Sprites/Message Icons/" + messageName + ".png", typeof(Sprite));

		Selection.activeTransform.Find("Panel/Message/Text").GetComponent<Text>().text = newMessage;

		UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
	}
}